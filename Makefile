FILE    = talk
LHS2TEX = lhs2TeX -v --tt
LATEXMK = latexmk -pdf
RM      = rm -f
RSVG    = rsvg-convert --format=pdf

LHSRCS = \

LHFORMATS = \
	talk.fmt
	
TEXSRCS = \
	preamble.tex

SVGFIGURES = \
	figures/pipeline.svg \
	figures/mac.svg \
	figures/smac.svg \
	figures/cpu.svg \
	figures/reducer.svg \

default: $(FILE)

$(FILE): texs figs $(TEXSRCS) $(LHFORMATS) $(FILE).tex
	$(LATEXMK) $(FILE); \

texs : $(LHSRCS:.lhs=.tex) 
%.tex : %.lhs
	$(LHS2TEX) $< > $@

figs : $(SVGFIGURES:.svg=.pdf)
%.pdf : %.svg
	$(RSVG) $< > $@

clean:
	latexmk -C talk
	$(RM) $(SVGFIGURES:.svg=.pdf)
	$(RM) $(FILE).tex
	$(RM) $(FILE).ptb
	$(RM) $(FILE).synctex.gz
	$(RM) $(FILE).nav
	$(RM) $(FILE).snm
	$(RM) *.hi *.o *.aux
